package com.demo.springboot.service;

import com.demo.springboot.domain.dto.MovieListDto;
import com.demo.springboot.domain.dto.UpdateMovie;
import org.springframework.http.ResponseEntity;

public interface MovieService {
    MovieListDto openFile();
    ResponseEntity updateMovieInFileCsv(UpdateMovie updateMovie, String id);
}
